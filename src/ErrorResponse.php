<?php
declare(strict_types=1);

namespace Iraon\Laravel\Responses;

final class ErrorResponse extends BaseResponse
{
    /**
     * @var ErrorDto[]
     */
    private array $errors = [];

    /**
     * @param  int     $code
     * @param  string  $message
     * @param  string  $details
     *
     * @return static
     */
    public function addError(int $code, string $message, string $details = ''): static
    {
        $errorDto          = new ErrorDto();
        $errorDto->code    = $code;
        $errorDto->message = $message;
        $errorDto->details = $details;

        $this->errors[] = $errorDto;

        return $this;
    }

    /**
     * @return array[]
     */
    protected function prepareDataForResponse(): array
    {
        return [
            'errors' => array_map(function (ErrorDto $errorDto) {
                return [
                    'code'    => $errorDto->code,
                    'message' => $errorDto->message,
                    'detail'  => $errorDto->details,
                ];
            }, $this->errors)
        ];
    }
}
