<?php
declare(strict_types=1);

namespace Iraon\Laravel\Responses;

class ErrorDto
{
    public int $code;

    public string $message;

    public string $details = '';
}
