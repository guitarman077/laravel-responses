<?php
declare(strict_types=1);

namespace Iraon\Laravel\Responses;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseResponse
{
    public static function create(): static
    {
        return new static();
    }

    /**
     * @param  int  $responseStatus
     *
     * @return JsonResponse
     */
    public function asJson(int $responseStatus = Response::HTTP_OK): JsonResponse
    {
        $responseData = $this->prepareDataForResponse();

        return response()->json($responseData)->setStatusCode($responseStatus);
    }

    /**
     * @return array
     */
    abstract protected function prepareDataForResponse(): array;
}
