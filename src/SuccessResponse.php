<?php
declare(strict_types=1);

namespace Iraon\Laravel\Responses;

final class SuccessResponse extends BaseResponse
{
    /**
     * @var WarningDto[]
     */
    private array $warnings = [];

    private array $result = [];

    /**
     * @param  int     $code
     * @param  string  $message
     * @param  string  $details
     *
     * @return static
     */
    public function addWarning(int $code, string $message, string $details = ''): static
    {
        $warningDto          = new WarningDto();
        $warningDto->code    = $code;
        $warningDto->message = $message;
        $warningDto->details = $details;

        $this->warnings[] = $warningDto;

        return $this;
    }

    /**
     * @param  array  $data
     *
     * @return static
     */
    public function setResult(array $data): static
    {
        $this->result = $data;

        return $this;
    }

    /**
     * @return array
     */
    protected function prepareDataForResponse(): array
    {
        $responseData = [];

        if (count($this->warnings) > 0) {
            $responseData['warnings'] = array_map(function (WarningDto $warningDto) {
                return [
                    'code'    => $warningDto->code,
                    'message' => $warningDto->message,
                    'detail'  => $warningDto->details,
                ];
            }, $this->warnings);
        }

        $responseData['result'] = $this->result;

        return $responseData;
    }
}
